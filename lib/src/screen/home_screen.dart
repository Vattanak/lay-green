import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/controller/home_controller.dart';
import 'package:lay_green/src/screen/home_screen/widgets/category.dart';
import 'package:lay_green/src/screen/home_screen/widgets/slider.dart';
import 'package:lay_green/src/screen/home_screen/widgets/tabbar_listview.dart';
import 'package:lay_green/src/screen/home_screen/widgets/video_slider.dart';

class MyHomeScreen extends StatefulWidget {
  const MyHomeScreen({super.key});

  @override
  State<MyHomeScreen> createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 1, 73, 4),
          title: SizedBox(
            width: MediaQuery.of(context).size.width * 0.6,
            height: 40,
            child: TextField(
              cursorHeight: 20,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 5),
                fillColor: Colors.white,
                filled: true,
                hintText: 'Search',
                hintStyle: const TextStyle(
                  fontSize: 16,
                ),
                prefixIcon: const Icon(
                  Icons.search,
                  size: 22,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              _scaffoldKey.currentState?.openDrawer();
              print("open drawer");
            },
            icon: SvgPicture.asset(
              AssetPath.smallGrid,
              width: 20,
              height: 20,
            ),
          ),
          actions: [
            IconButton(
              onPressed: () {
              },
              icon: const Icon(
                Icons.notifications,
                color: Colors.white,
              ),
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Center(
                  child: Icon(Icons.person)
                ),
              ),
              ListTile(
                title: Text('Item 1'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: Text('Item 2'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              // Add more list items as needed
            ],
          ),
        ),
        body: Stack(children: [
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AssetPath.appBackGround), fit: BoxFit.cover),
            ),
          ),
          GetBuilder<HomeController>(builder: (controller) => 
            SizedBox(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                shrinkWrap: true,
                children: [
                  //category
                  CategoryFixed(categoryList: controller.categoryList),
                  //slider
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SizedBox(
                      height: 150,
                      width: MediaQuery.of(context).size.width,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: HomeSlider(sliderList: controller.sliderList),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Video",style: TextStyle(fontWeight: FontWeight.bold),),
                        Text(
                          "See All",
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  //video
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SizedBox(
                      height: 150,
                      width: MediaQuery.of(context).size.width,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: VideoSlider(videoList: controller.videoList),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  DefaultTabController(
                    length: 3,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 50,
                          child: TabBar(
                            onTap: (value) {
                              value == 0
                                  ? controller.getProductData(tab: "latest")
                                  : value == 1
                                      ? controller.getProductData(
                                          tab: "best_sale")
                                      : value == 2
                                          ? controller.getProductData(
                                              tab: "promotion")
                                          : null;
                            },
                            tabs: const [
                              Tab(
                                text: "Latest",
                              ),
                              Tab(
                                text: "Best Sale",
                              ),
                              Tab(
                                text: "Promotion",
                              ),
                            ],
                          ),
                        ),
                        Obx(() => 
                          Padding(
                            padding: EdgeInsets.symmetric(vertical:8.0,horizontal: 16),
                            child: controller.isLoading.value
                                ? const Center(
                                    child: CircularProgressIndicator())
                                // :controller.productList.isEmpty ? Center()
                                : TabBarListView(
                                    itemCount: controller.productList.length,
                                  ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),)
          ]
        ),
        bottomNavigationBar: SizedBox(
          width: double.maxFinite,
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                onPressed: () {},
                icon: ColorFiltered(
                  colorFilter:
                      const ColorFilter.mode(Color.fromARGB(255, 1, 73, 4), BlendMode.srcIn),
                  child: SvgPicture.asset(AssetPath.iconHome),
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(AssetPath.iconChat),
              ),
              IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(AssetPath.iconFavorite),
              ),
              IconButton(
                onPressed: () {},
                icon: Container(
                  width: 27,
                  height: 27,
                  child: SvgPicture.asset(
                    AssetPath.iconAccount,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
