class HomeSreenModel {
  bool? success;
  Data? data;
  String? message;

  HomeSreenModel({this.success, this.data, this.message});

  HomeSreenModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['success'] = success;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = message;
    return data;
  }
}

class Data {
  List<Banners>? banners;
  List<Videos>? videos;
  List<Categories>? categories;
  Null notification;

  Data({this.banners, this.videos, this.categories, this.notification});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['banners'] != null) {
      banners = <Banners>[];
      json['banners'].forEach((v) {
        banners!.add(Banners.fromJson(v));
      });
    }
    if (json['videos'] != null) {
      videos = <Videos>[];
      json['videos'].forEach((v) {
        videos!.add(Videos.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = <Categories>[];
      json['categories'].forEach((v) {
        categories!.add(Categories.fromJson(v));
      });
    }
    notification = json['notification'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (banners != null) {
      data['banners'] = banners!.map((v) => v.toJson()).toList();
    }
    if (videos != null) {
      data['videos'] = videos!.map((v) => v.toJson()).toList();
    }
    if (categories != null) {
      data['categories'] = categories!.map((v) => v.toJson()).toList();
    }
    data['notification'] = notification;
    return data;
  }
}

class Banners {
  String? id;
  String? image;

  Banners({this.id, this.image});

  Banners.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['image'] = image;
    return data;
  }
}

class Videos {
  String? id;
  String? videoUrl;
  String? thumbnail;

  Videos({this.id, this.videoUrl, this.thumbnail});

  Videos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    videoUrl = json['video_url'];
    thumbnail = json['thumbnail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['video_url'] = videoUrl;
    data['thumbnail'] = thumbnail;
    return data;
  }
}

class Categories {
  String? id;
  String? name;
  String? image;
  String? parentCategory;
  Null createdAt;
  String? updatedAt;
  String? nameKh;
  String? status;
  Null code;
  String? hasSubcategory;

  Categories(
      {this.id,
      this.name,
      this.image,
      this.parentCategory,
      this.createdAt,
      this.updatedAt,
      this.nameKh,
      this.status,
      this.code,
      this.hasSubcategory});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    parentCategory = json['parent_category'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    nameKh = json['name_kh'];
    status = json['status'];
    code = json['code'];
    hasSubcategory = json['has_subcategory'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['parent_category'] = parentCategory;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['name_kh'] = nameKh;
    data['status'] = status;
    data['code'] = code;
    data['has_subcategory'] = hasSubcategory;
    return data;
  }
}
