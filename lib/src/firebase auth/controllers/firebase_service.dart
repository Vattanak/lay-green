import 'dart:developer';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/firebase%20auth/repository/firebase_login_repo.dart';
import 'package:lay_green/src/firebase%20auth/screens/otp_screen.dart';
import 'package:lay_green/src/screen/home_screen.dart';

class FirebaseAuthController extends GetxController{
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final phoneController = TextEditingController();
  final otpController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  var firebaseRepo =  Get.put(FirebaseLoginRepo());
  late Rx<User?> _firebaseUser;




  Future<void> logInWihPhoneNumber() async {
    firebaseRepo.phoneAuthentication("+855${phoneController.text}").then(
          (_) => Get.to(const OtpScreen()),
        );
  }

  void verifyOtp() async {
    bool otpSuccess = await firebaseRepo.verifyOtpRepo(otpController.text);
    if (otpSuccess == true) {
      Get.offAll(MyHomeScreen());      
    }else{
      null;
    }
  }
}