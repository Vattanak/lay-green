import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:lay_green/src/constant/api_path.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/controller/home_controller.dart';
import 'package:lay_green/src/model/home_model.dart';

// ignore: must_be_immutable
class VideoSlider extends StatefulWidget {
  List<Videos> videoList;
  VideoSlider({super.key, required this.videoList});
  @override
  State<VideoSlider> createState() => _VideoSliderState();
}

class _VideoSliderState extends State<VideoSlider> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      Stack(
        children: [
          Container(
            width: double.maxFinite,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: CarouselSlider(
              options: CarouselOptions(
                viewportFraction: 1.0,
              ),
              items: widget.videoList
                      .map(
                        (item) => GestureDetector(
                          onTap: () {
                            item.videoUrl;
                          },
                          child: SizedBox(
                            width: double.maxFinite,
                            child: Stack(
                              children: [
                                Center(
                                  child: CachedNetworkImage(
                                    width: double.maxFinite,
                                    height: double.maxFinite,
                                    imageUrl: ApiPath.baseUrl+item.thumbnail!.toString(),
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) => SvgPicture.asset(
                                      AssetPath.videoPlaceHolder,
                                      fit: BoxFit.cover,
                                    ),
                                    errorWidget: (context, url, error) =>
                                        const Icon(Icons.error),
                                  ),
                                ),
                                Center(child: Image.asset(AssetPath.iconPlayButton,width: 100,height: 100,)),
                              ],
                            ),
                          ),
                        ),
                      )
                      .toList(),
          )),
        ],
      ),
    );
  }
}