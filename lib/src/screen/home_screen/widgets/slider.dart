import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/model/home_model.dart';

// ignore: must_be_immutable
class HomeSlider extends StatefulWidget {
  List<Banners> sliderList;
  HomeSlider({super.key, required this.sliderList});
  @override
  State<HomeSlider> createState() => _HomeSliderState();
}

class _HomeSliderState extends State<HomeSlider> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.maxFinite,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlayAnimationDuration: const Duration(milliseconds: 500),
                autoPlay: true,
                viewportFraction: 1.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }
              ),
              items: widget.sliderList
                      .map(
                        (item) => SizedBox(
                          width: double.maxFinite,
                          child: CachedNetworkImage(
                          imageUrl: item.image!.toString(),
                          fit: BoxFit.cover,
                          placeholder: (context, url) =>
                              SvgPicture.asset(AssetPath.bannerPlaceHolder,fit: BoxFit.cover,),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        ),
                        ),
                      )
                      .toList(),
          )),
          Positioned(
            bottom: 0,
            right: 10,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widget.sliderList.asMap().entries.map((entry) {
                return Container(
                  width: 12.0,
                  height: 12.0,
                  margin: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 4.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: (Theme.of(context).brightness == Brightness.dark
                              ? Colors.white
                              : Colors.black)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}