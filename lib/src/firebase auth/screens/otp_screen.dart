import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/firebase%20auth/controllers/firebase_service.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class OtpScreen extends StatelessWidget {
  const OtpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<FirebaseAuthController>(builder: (controller) {
        return GestureDetector(
          onTap: () => FocusManager().primaryFocus?.unfocus(),
          child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(AssetPath.appBackGround),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.20,
                      width: double.maxFinite,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  padding: const EdgeInsets.only(top: 30),
                                  height: 60,
                                  width: 210,
                                  // color: Colors.yellow,
                                  child: const Text(
                                    "Enter Code",
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(top: 10),
                                  // color: Colors.red,
                                  height: 60,
                                  width: 210,
                                  child: const Text(
                                    "Enter the code , we send to your phone please check and fill it.",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(child: Container()),
                            GestureDetector(
                              onTap: () => Navigator.of(context).pop(),
                              child: Container(
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: const Color.fromARGB(255, 39, 53, 39),
                                ),
                                child: const Center(
                                  child: Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: double.maxFinite,
                      padding: const EdgeInsets.only(left: 20),
                      child: const Text(
                        "+855967087889",
                        style:
                            TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: 20,),
                    Form(
                      key: controller.formKey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 8.0,
                          horizontal: 30,
                        ),
                        child: PinCodeTextField(
                          controller: controller.otpController,
                          onCompleted: (value) {
                            controller.otpController.text = value;
                          },
                          keyboardType: TextInputType.number,
                          appContext: context,
                          length: 6,
                          pinTheme: PinTheme(
                            selectedFillColor: Colors.white,
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(5),
                            fieldHeight: 50,
                            fieldWidth: 40,
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "OTP can't be empty";
                            }
                            return null;
                          },
                          beforeTextPaste: (text) {
                            debugPrint("Allowing to paste $text");
                            //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                            //but you can show anything you want here, like your pop up saying wrong paste format or etc
                            return true;
                          },
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Text("60s"),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: () {
                        controller.verifyOtp();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 1, 73, 4),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        width: 125,
                        height: 50,
                        child: const Center(
                          child: Text(
                            "Verify",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
        );
        },
      ),
    );
  }
}