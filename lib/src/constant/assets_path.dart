
class AssetPath{
  //images
  static String smallGrid = "assets/images/best_image/svg_drawer.svg";
  static String nike = "assets/images/best_image/nike.jpg";
  static String adidas = "assets/images/best_image/adidas.jpg";
  static String puma = "assets/images/best_image/puma.png";
  static String appBackGround = "assets/images/best_image/background.jpg";
  static String bannerPlaceHolder = "assets/images/best_image/banner_placeholder.svg";
  static String videoPlaceHolder = "assets/images/best_image/video_placeholder.svg";
  static String productPlaceHolder = "assets/images/best_image/product_placeholder.svg";
  //icons
  static String iconPlayButton = "assets/icons/icon_play.png";
  static String iconHome = "assets/icons/icon_home_main.svg";
  static String iconChat = "assets/icons/icon_chat.svg";
  static String iconFavorite = "assets/icons/icon_favorite.svg";
  static String iconProfile = "assets/icons/icon_profile.svg";
  static String iconAccount = "assets/icons/icon_account.svg";
  static String iconHeartBox = "assets/icons/icon_heart_box.svg";
  static String cambodiaFlag = "assets/icons/cambodia.svg";
}