// To parse this JSON data, do
//
//     final productDetailModel = productDetailModelFromJson(jsonString);

import 'dart:developer';

import 'package:meta/meta.dart';
import 'dart:convert';

ProductDetailModel productDetailModelFromJson(String str) => ProductDetailModel.fromJson(json.decode(str));

String productDetailModelToJson(ProductDetailModel data) => json.encode(data.toJson());

class ProductDetailModel {
    bool success;
    Data data;
    String message;

    ProductDetailModel({
        required this.success,
        required this.data,
        required this.message,
    });

    factory ProductDetailModel.fromJson(Map<String, dynamic> json) => ProductDetailModel(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
        "message": message,
    };
}

class Data {
    Product product;
    List<Product> relatedProduct;

    Data({
        required this.product,
        required this.relatedProduct,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        product: Product.fromJson(json["product"]),
        relatedProduct: List<Product>.from(json["related_product"].map((x) => Product.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "product": product.toJson(),
        "related_product": List<dynamic>.from(relatedProduct.map((x) => x.toJson())),
    };
}

class Product {
    int id;
    String code;
    String name;
    String description;
    String image;
    dynamic videoUrl;
    dynamic inStock;
    String isFeature;
    String status;
    // DateTime createdAt;
    // DateTime updatedAt;
    dynamic deletedAt;
    String nameKh;
    String descriptionKh;
    String categoryId;
    String subCategory;
    dynamic brand;
    dynamic videoThumbnail;
    dynamic finishedId;
    dynamic seriesId;
    dynamic sizeId;
    dynamic colorId;
    List<dynamic>? galleries;
    List<dynamic> documents;

    Product({
        required this.id,
        required this.code,
        required this.name,
        required this.description,
        required this.image,
        required this.videoUrl,
        required this.inStock,
        required this.isFeature,
        required this.status,
        // required this.createdAt,
        // required this.updatedAt,
        required this.deletedAt,
        required this.nameKh,
        required this.descriptionKh,
        required this.categoryId,
        required this.subCategory,
        required this.brand,
        required this.videoThumbnail,
        required this.finishedId,
        required this.seriesId,
        required this.sizeId,
        required this.colorId,
        required this.galleries,
        required this.documents,
    });

    factory Product.fromJson(Map<String, dynamic> json){
      return
      Product(
        id: json["id"],
        code: json["code"],
        name: json["name"],
        description: json["description"],
        image: json["image"],
        videoUrl: json["video_url"],
        inStock: json["in_stock"],
        isFeature: json["is_feature"],
        status: json["status"],
        // createdAt: DateTime.parse(json["created_at"]),
        // updatedAt: DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        nameKh: json["name_kh"],
        descriptionKh: json["description_kh"],
        categoryId: json["category_id"],
        subCategory: json["sub_category"],
        brand: json["brand"],
        videoThumbnail: json["video_thumbnail"],
        finishedId: json["finished_id"],
        seriesId: json["series_id"],
        sizeId: json["size_id"],
        colorId: json["color_id"],
        galleries: json["galleries"] == null ? <dynamic>[]:List<dynamic>.from(json["galleries"].map((x) => x)),
        documents: json["galleries"] == null ? <dynamic>[]:List<dynamic>.from(json["documents"].map((x) => x)),
    );
    } 

    Map<String, dynamic> toJson() => {
        "id": id,
        "code": code,
        "name": name,
        "description": description,
        "image": image,
        "video_url": videoUrl,
        "in_stock": inStock,
        "is_feature": isFeature,
        "status": status,
        // "created_at": createdAt.toIso8601String(),
        // "updated_at": updatedAt.toIso8601String(),
        "deleted_at": deletedAt,
        "name_kh": nameKh,
        "description_kh": descriptionKh,
        "category_id": categoryId,
        "sub_category": subCategory,
        "brand": brand,
        "video_thumbnail": videoThumbnail,
        "finished_id": finishedId,
        "series_id": seriesId,
        "size_id": sizeId,
        "color_id": colorId,
        "galleries": List<dynamic>.from(galleries!.map((x) => x)),
        "documents": List<dynamic>.from(documents.map((x) => x)),
    };
}
