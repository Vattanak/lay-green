import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/firebase%20auth/screens/otp_screen.dart';
import 'package:lay_green/src/firebase%20auth/screens/phone_login.dart';
import 'package:lay_green/src/screen/home_screen.dart';

class FirebaseLoginRepo extends GetxController{
  FirebaseAuth _auth = FirebaseAuth.instance;
  String _verificationId = '';
  late final Rx<User?> _firebaseUser;

  // Future<void> logWithPhone(String phoneController)async{
  //   await _auth.verifyPhoneNumber(
  //     phoneNumber: "+855$phoneController",
  //     timeout: const Duration(seconds: 60),
  //     verificationCompleted: (PhoneAuthCredential credential) async {
  //       // Auto-retrieval of SMS code completed (not usually needed)
  //       await _auth.signInWithCredential(credential);
  //     },
  //     verificationFailed: (FirebaseAuthException e) {
  //       Fluttertoast.showToast(msg: 'Verification failed: ${e.message}');
  //       log(e.message.toString());
  //     },
  //     codeSent: (String verificationId, int? resendToken) {
  //       _verificationId = verificationId;
  //     },
  //     codeAutoRetrievalTimeout: (String verificationId) {
  //       _verificationId = verificationId;
  //     },
  //   );
  // }

  @override
  void onReady() {
    _firebaseUser = Rx<User?>(_auth.currentUser);
    _firebaseUser.bindStream(_auth.userChanges());
    setInitialScreen(_firebaseUser.value);
    // ever(_firebaseUser, _setInitialScreen);
  }

  setInitialScreen(User? user) async {
    if (user == null) {
      Get.offAll(() => const PhoneLoginScreen()); //WelcomeScreen())
    } else {
      Get.offAll(() => const MyHomeScreen());
    }
  }

  Future<void> phoneAuthentication(String phoneNo) async {
    try {
      await _auth.verifyPhoneNumber(
        phoneNumber: phoneNo,
        timeout: Duration(seconds: 60),
        verificationCompleted: (credential) async {
          await _auth.signInWithCredential(credential);
        },
        codeSent: (verificationId, resendToken) {
          _verificationId = verificationId;
        },
        codeAutoRetrievalTimeout: (verificationId) {
          _verificationId = verificationId;
        },
        verificationFailed: (e) {
          Fluttertoast.showToast(msg: 'Verification failed: ${e.message}');
          log(e.message.toString());
        },
      );
    } on FirebaseAuthException catch (e) {
      throw "${e.message}";
    } catch (e) {
      throw e.toString().isEmpty ? 'Unknown Error Occurred. Try again!' : e.toString();
    }
  }

  Future<bool> verifyOtpRepo(String smsCode) async{
    try {
      AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: smsCode,
      );
      UserCredential result = await _auth.signInWithCredential(credential);
      User? user = result.user;
      if (user != null) {
        Fluttertoast.showToast(msg: 'Logged in as ${user.phoneNumber}');
        return true;
      } else {
        Fluttertoast.showToast(msg: 'Failed to authenticate');
        return false;
      }
    } catch (e) {
      Fluttertoast.showToast(msg: 'Error signing in: $e');
      return false;
    }
  }
    
}