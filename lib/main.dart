import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lay_green/example.dart';
import 'package:lay_green/src/firebase%20auth/screens/otp_screen.dart';
import 'package:lay_green/src/firebase%20auth/screens/phone_login.dart';
import 'package:lay_green/src/screen/home_screen.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color.fromARGB(255, 1, 73, 4)),
        useMaterial3: true,
      ),
      home: const MyHomeScreen(),
    );
  }
}