import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/constant/api_path.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/controller/home_controller.dart';
import 'package:lay_green/src/model/home_model.dart';

// ignore: must_be_immutable
class CategoryFixed extends StatelessWidget {
  List<Categories> categoryList;
  CategoryFixed({
    super.key,
    required this.categoryList
  });

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: 100,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color:Colors.grey.withOpacity(0.5), 
                  spreadRadius: 2, 
                  blurRadius: 5, 
                  offset: const Offset(0, 2),
                ),
              ]
            ),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const SizedBox(width: 5,),
                SizedBox(
                  width: 20,
                  height: 20,
                  child: controller.categoryList.isEmpty
                        ? const CircularProgressIndicator(
                            color: Color.fromARGB(255, 1, 73, 4),
                            strokeWidth: 2,
                          )
                        : CachedNetworkImage(
                            imageUrl: ApiPath.baseUrl +
                                controller.categoryList[0].image!,
                            // AssetPath.nike,
                            fit: BoxFit.cover,
                          ),
                ),
                const SizedBox(width: 5,),
                SizedBox(
                  width: 70,
                  child: Text(
                    controller.categoryList.isEmpty?"":controller.categoryList[2].name!,
                    // "nike",
                    style: const TextStyle(color: Colors.black, fontSize: 8),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 100,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color:
                      Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset:
                      const Offset(0, 2),
                ),
              ]
            ),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const SizedBox(width: 5,),
                SizedBox(
                  width: 20,
                  height: 20,
                  child: controller.categoryList.isEmpty
                        ? const CircularProgressIndicator(
                            color: Color.fromARGB(255, 1, 73, 4),
                            strokeWidth: 2,
                          )
                        : CachedNetworkImage(
                            imageUrl: ApiPath.baseUrl +
                                controller.categoryList[1].image!,
                            // AssetPath.adidas,
                            fit: BoxFit.cover,
                          ),
                ),
                const SizedBox(width: 5,),
                SizedBox(
                  width: 70,
                  child: Text(
                    controller.categoryList.isEmpty?"":controller.categoryList[1].name!,
                    // "adidas",
                    style: const TextStyle(color: Colors.black, fontSize: 8),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 100,
              // padding: EdgeInsets.symmetric(horizontal: 5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color:
                      Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset:
                      const Offset(0, 2),
                ),
              ]
            ),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const SizedBox(width: 5,),
                SizedBox(
                  width: 20,
                  height: 20,
                  child: controller.categoryList.isEmpty
                        ? const CircularProgressIndicator(
                            color: Color.fromARGB(255, 1, 73, 4),
                            strokeWidth: 2,
                          )
                        : CachedNetworkImage(
                            imageUrl: ApiPath.baseUrl +
                                controller.categoryList[2].image!,
                            // AssetPath.puma,
                          ),
                ),
                const SizedBox(width: 5,),
                SizedBox(
                  width: 70,
                  child: Text(
                    controller.categoryList.isEmpty?"":controller.categoryList[2].name!,
                    // "puma",
                    style: const TextStyle(color: Colors.black, fontSize: 8),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    } );
    
  }
}