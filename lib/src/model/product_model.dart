class ProductsModel {
  bool? success;
  Data? data;
  String? message;

  ProductsModel({this.success, this.data, this.message});

  ProductsModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  List<Products>? products;
  Pagination? pagination;

  Data({this.products, this.pagination});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = <Products>[];
      json['products'].forEach((v) {
        products!.add(new Products.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.products != null) {
      data['products'] = this.products!.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination!.toJson();
    }
    return data;
  }
}

class Products {
  String? discount;
  String? discountType;
  String? id;
  String? code;
  String? name;
  String? description;
  String? image;
  Null? videoUrl;
  Null? inStock;
  String? isFeature;
  String? status;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;
  String? nameKh;
  String? descriptionKh;
  String? categoryId;
  String? subCategory;
  Null? brand;
  Null? videoThumbnail;
  String? finishedId;
  String? seriesId;
  String? sizeId;
  Null? colorId;
  int? isFavorite;

  Products(
      {this.discount,
      this.discountType,
      this.id,
      this.code,
      this.name,
      this.description,
      this.image,
      this.videoUrl,
      this.inStock,
      this.isFeature,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.nameKh,
      this.descriptionKh,
      this.categoryId,
      this.subCategory,
      this.brand,
      this.videoThumbnail,
      this.finishedId,
      this.seriesId,
      this.sizeId,
      this.colorId,
      this.isFavorite});

  Products.fromJson(Map<String, dynamic> json) {
    discount = json['discount'];
    discountType = json['discount_type'];
    id = json['id'].toString();
    code = json['code'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    videoUrl = json['video_url'];
    inStock = json['in_stock'];
    isFeature = json['is_feature'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    nameKh = json['name_kh'];
    descriptionKh = json['description_kh'];
    categoryId = json['category_id'];
    subCategory = json['sub_category'];
    brand = json['brand'];
    videoThumbnail = json['video_thumbnail'];
    finishedId = json['finished_id'];
    seriesId = json['series_id'];
    sizeId = json['size_id'];
    colorId = json['color_id'];
    isFavorite = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['discount'] = this.discount;
    data['discount_type'] = this.discountType;
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['description'] = this.description;
    data['image'] = this.image;
    data['video_url'] = this.videoUrl;
    data['in_stock'] = this.inStock;
    data['is_feature'] = this.isFeature;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['name_kh'] = this.nameKh;
    data['description_kh'] = this.descriptionKh;
    data['category_id'] = this.categoryId;
    data['sub_category'] = this.subCategory;
    data['brand'] = this.brand;
    data['video_thumbnail'] = this.videoThumbnail;
    data['finished_id'] = this.finishedId;
    data['series_id'] = this.seriesId;
    data['size_id'] = this.sizeId;
    data['color_id'] = this.colorId;
    data['is_favorite'] = this.isFavorite;
    return data;
  }
}

class Pagination {
  int? totalRecords;
  int? totalPages;
  int? page;
  int? perPage;
  String? firstUrl;
  String? currentUrl;
  String? nextUrl;
  String? lastUrl;

  Pagination(
      {this.totalRecords,
      this.totalPages,
      this.page,
      this.perPage,
      this.firstUrl,
      this.currentUrl,
      this.nextUrl,
      this.lastUrl});

  Pagination.fromJson(Map<String, dynamic> json) {
    totalRecords = json['total_records'];
    totalPages = json['total_pages'];
    page = json['page'];
    perPage = json['per_page'];
    firstUrl = json['first_url'];
    currentUrl = json['current_url'];
    nextUrl = json['next_url'];
    lastUrl = json['last_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_records'] = this.totalRecords;
    data['total_pages'] = this.totalPages;
    data['page'] = this.page;
    data['per_page'] = this.perPage;
    data['first_url'] = this.firstUrl;
    data['current_url'] = this.currentUrl;
    data['next_url'] = this.nextUrl;
    data['last_url'] = this.lastUrl;
    return data;
  }
}
