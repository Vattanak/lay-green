import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/constant/api_path.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/controller/home_controller.dart';
import 'package:lay_green/src/model/product_model.dart';

class ProductDetailScreen extends StatelessWidget {
  Products product;
  ProductDetailScreen({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail",style: TextStyle(color: Colors.white),),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: IconButton(onPressed: () => Navigator.of(context).pop(),icon: const Icon(Icons.arrow_back,color: Colors.white,),),
        backgroundColor: const Color.fromARGB(255, 1, 73, 4),
      ),
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AssetPath.appBackGround), fit: BoxFit.cover),
            ),
          ),
          GetBuilder<HomeController>(builder: (controller) {
              return ListView(
                children: [
                  //image
                  Container(
                    height: MediaQuery.of(context).size.height * 0.27,
                    width: double.maxFinite,
                    child: 
                    CachedNetworkImage(
                      imageUrl: ApiPath.baseUrl + product.image!,
                      fit: BoxFit.fill,
                    ),
                  ),
                  const SizedBox(height: 10,),
                  //dot
                  Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color.fromARGB(255, 1, 73, 4),
                    )
                  ),
                  const SizedBox(height: 20,),
                  //product name
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      product.name!,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const SizedBox(height: 20,),
                  //discription
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      product.descriptionKh!,
                      style: const TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                  const SizedBox(height: 20,),
                  //download hd image
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      padding: const EdgeInsets.only(right: 10),
                      margin: const EdgeInsets.only(right: 150),
                      width: 50,
                      height: 30,
                      decoration: BoxDecoration(color: const Color.fromARGB(255, 1, 73, 4), borderRadius: BorderRadius.circular(5)),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "Download HD Images",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 10,),
                  //related
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: [
                        Text(
                          "Related Items",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20,),
                  //related item listview
                  Container(
                    height: 200,
                    width: double.maxFinite,
                    // color: Colors.red,
                    child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      scrollDirection: Axis.horizontal,
                      itemCount: controller.relatedProduct.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Column(
                              children: [
                                Container(
                                  height: 160 * 0.5,
                                  width: 120,
                                  child: controller.relatedProduct.isEmpty
                                      ? const Center(
                                          child: CircularProgressIndicator())
                                      : CachedNetworkImage(
                                          imageUrl: ApiPath.baseUrl +
                                              controller
                                                  .relatedProduct[index].image,
                                          fit: BoxFit.fill,
                                        ),
                                ),
                                Stack(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(8),
                                      height: 150 * 0.5,
                                      width: 120,
                                      color: Colors.white,
                                      child: Text(
                                        controller.relatedProduct.isEmpty
                                            ? ""
                                            : controller
                                                .relatedProduct[index].name,
                                        style: const TextStyle(fontSize: 8),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 5,
                                      right: 5,
                                      child: Container(
                                        height: 30,
                                        width: 30,
                                        child: ColorFiltered(
                                          colorFilter: const ColorFilter.mode(
                                            Color.fromARGB(255, 1, 73, 4),
                                            BlendMode.srcIn,
                                          ),
                                          child: SvgPicture.asset(
                                              AssetPath.iconHeartBox),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            },),
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: 70,
        width: double.maxFinite,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: Color.fromARGB(255, 39, 53, 39),
                ),
                child: Center(child: Icon(Icons.chat_outlined,color: Colors.white, size: 20,)),
              ),
              Container(
                width: 200,
                height: double.maxFinite,
                padding: EdgeInsets.symmetric(horizontal: 35),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: Color.fromARGB(255, 1, 73, 4),
                ),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.favorite,color: Colors.white,),
                    Text("Favorite",style: TextStyle(color: Colors.white, fontSize: 12),),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}