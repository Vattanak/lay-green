import 'package:get/get.dart';
import 'package:lay_green/src/constant/api_path.dart';
import 'package:lay_green/src/repository/service.dart';

class HomeRepo extends GetxService{
  final apiService = Get.put(ApiService());

  Future<Response> getData()async{
    return await apiService.getData(ApiPath.home);
  }

  Future<Response> getProductData(String tab)async{
    String param = "?page=1&per_page=10&type=$tab&user_id&is_login=false";
    return await apiService.getData(ApiPath.products+param);
  }
  
  Future<Response> getProductDetail(String id)async{
    return await apiService.getData(ApiPath.productDetail+id);
  }
}