import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lay_green/src/constant/api_path.dart';
import 'package:lay_green/src/constant/assets_path.dart';
import 'package:lay_green/src/controller/home_controller.dart';
import 'package:lay_green/src/model/product_model.dart';
import 'package:lay_green/src/screen/product_detail_screen.dart';

class TabBarListView extends StatelessWidget {
  final int itemCount;
  TabBarListView({super.key, required this.itemCount});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: itemCount,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisExtent: 150,
        mainAxisSpacing: 12,
        crossAxisSpacing: 10,
        childAspectRatio: 0.9,
      ),
      itemBuilder: (context, index) {
        return GetBuilder<HomeController>(builder: (controller) {
          return GestureDetector(
              onTap: () {
                controller.getProductDetail(controller.productList[index].id!);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProductDetailScreen(product: controller.productList[index]),
                  ),
                );
                
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Column(
                    children: [
                      Container(
                        height: 150 * 0.6,
                        width: double.maxFinite,
                        child: controller.productList.isEmpty
                            ? const Center(child: CircularProgressIndicator())
                            : CachedNetworkImage(
                                imageUrl:
                                    ApiPath.baseUrl + controller.productList[index].image!,
                                fit: BoxFit.fill,
                              ),
                      ),
                      Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.all(8),
                            height: 150 * 0.4,
                            width: double.maxFinite,
                            color: Colors.white,
                            child: Text(
                              controller.productList.isEmpty
                                  ? ""
                                  : controller.productList[index].name!,
                              style: const TextStyle(fontSize: 10),
                            ),
                          ),
                          Positioned(
                              bottom: 7,
                              right: 5,
                              child: Container(
                                height: 30,
                                width: 30,
                                child: ColorFiltered(
                                  colorFilter: const ColorFilter.mode(
                                    Color.fromARGB(255, 1, 73, 4),
                                    BlendMode.srcIn,
                                  ),
                                  child:
                                      SvgPicture.asset(AssetPath.iconHeartBox),
                                ),
                              )),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );
        },);
        
      },
    );





    
  //   GridView.custom(
  //     shrinkWrap: true,
  //     physics: NeverScrollableScrollPhysics(),
  //     gridDelegate: SliverWovenGridDelegate.count(
  //       crossAxisCount: 2,
  //       mainAxisSpacing: 8,
  //       crossAxisSpacing: 8,
  //       pattern: [
  //         const WovenGridTile(1),
  //         const WovenGridTile(
  //           0.9,
  //           crossAxisRatio: 1,
            
  //           alignment: AlignmentDirectional.centerEnd,
            
  //         ),
  //       ],
  //     ),
  //     childrenDelegate: SliverChildBuilderDelegate(
  //       (context, index) {
  //         // Example: Display different widgets based on index
  //         if (index == 0) {
  //           return ClipRRect(
  //             borderRadius: BorderRadius.circular(10),
  //             child: Container(
  //               color: Colors.red,
  //               // Widget for index 0
  //             ),
  //           );
  //         } else if (index == 1) {
  //           return ClipRRect(
  //             borderRadius: BorderRadius.circular(10),
  //             child: Container(
  //               color: Colors.blue,
  //               // Widget for index 0
  //             ),
  //           );
  //         } else {
  //           return ClipRRect(
  //             borderRadius: BorderRadius.circular(10),
  //             child: Container(
  //               color: Colors.green,
  //               // Widget for index 0
  //             ),
  //           );
  //         }
  //       },
  //       childCount: 10, // Replace with your desired number of grid items
  //     ),
  //   );
  }
}