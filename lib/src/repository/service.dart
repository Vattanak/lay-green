import 'package:get/get.dart';
import 'package:lay_green/src/constant/api_path.dart';

class ApiService extends GetConnect implements GetxService{
  String appBaseUrl = ApiPath.baseUrl;
  late String token;

  ApiService(){
    appBaseUrl = appBaseUrl;
    timeout = const Duration(seconds: 30);
  }
  Future<Response> getData(String uri) async{
    try {
      Response response = await get(appBaseUrl+uri,headers: {'conten-type' : 'application/json; charset=UTF-8'});
      return response;
    } catch (e) {
      return Response(statusCode: 1,statusText: e.toString());
    }
  }
    
}