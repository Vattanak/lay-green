import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:lay_green/src/model/home_model.dart';
import 'package:lay_green/src/model/product_detail_model.dart';
import 'package:lay_green/src/model/product_model.dart';
import 'package:lay_green/src/repository/home_repo.dart';

class HomeController extends GetxController{
  final homeRepo = Get.put(HomeRepo());
  
  @override
  onInit(){
    super.onInit();
    getDataFromApi();
    getProductData();
  }
  
  var sliderList = List<Banners>.empty(growable: true);
  var videoList = List<Videos>.empty(growable: true);
  var categoryList= List<Categories>.empty(growable: true);
  Future<void> getDataFromApi() async{
    Response res = await homeRepo.getData();
    if (res.statusCode == 200) {
      final homeModel = HomeSreenModel.fromJson(res.body);
      sliderList.addAll(homeModel.data!.banners!);
      videoList.addAll(homeModel.data!.videos!);
      categoryList.addAll(homeModel.data!.categories!);
      update();
    }else{
      print(res.body);
      log(jsonEncode("api unsuccess!!"));

    }
  }

  var productList= List<Products>.empty(growable: true);
  RxBool isLoading = true.obs;
  Future<void> getProductData({String tab= "latest"}) async{
    isLoading.value = true;
    Response res = await homeRepo.getProductData(tab);
    if (res.statusCode == 200) {
      final productsModel = ProductsModel.fromJson(res.body);
      productList.clear();
      productList.addAll(productsModel.data!.products!);
      isLoading.value = false;
      update();
    }else{
      print(res.body);
      log(jsonEncode("api product unsuccess!!"));
    }
  }

  var relatedProduct = List<Product>.empty(growable: true);
  Future<void> getProductDetail(String id) async{
    print(id);
    Response res = await homeRepo.getProductDetail(id);
    if (res.statusCode == 200) {
      final productDetailModel = productDetailModelFromJson(jsonEncode(res.body));
      relatedProduct.clear();
      relatedProduct.addAll(productDetailModel.data.relatedProduct);
      update();
    }else{
      print(res.body);
      log(jsonEncode("api productdetail unsuccess!!"));
    }
  }
}